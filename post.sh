#!/bin/sh

node node_modules/roller-coaster-challenge-tools/src/cli.js || exit 1
node node_modules/roller-coaster-challenge-illustrator/illustrator.js track.json || exit 2
inkscape --export-png test.png --export-dpi 900 --export-background white test.svg || exit 3
pngcrush -c 2 test.png track.png || exit 4

#echo "Challenge for $(date -u +%Y-%m-%d)" > challenge-status.txt
#echo "Solution for $(date -u +%Y-%m-%d)" > solution-status.txt
echo "Track for $(date -u +%Y-%m-%d)" > track-status.txt

node post-on-mastodon.js || exit 5

rm test.png
rm test.svg
rm track.json
rm track.png
#rm challenge-status.txt
#rm solution-status.txt
rm track-status.txt
