import 'dotenv/config'
import fs from 'node:fs';
import Mastodon from '@jaller94/mastodon-api';

const M = new Mastodon({
  client_key: process.env.CLIENT_KEY,
  client_secret: process.env.CLIENT_SECRET,
  access_token: process.env.ACCESS_TOKEN,
  timeout_ms: 60 * 1000, // optional HTTP request timeout to apply to all requests.
  api_url: process.env.API_URL, // optional, defaults to https://mastodon.social/api/v1/
});

async function post() {
  const trackImgResp = await M.post('media', { file: fs.createReadStream('track.png') });
  const trackParams = {
    language: 'eng',
    status: fs.readFileSync('track-status.txt', 'utf-8').trim(),
    media_ids: [trackImgResp.data.id],
    sensitive: false,
    visibility: 'unlisted',
  };
  const trackPostResp = await M.post('statuses', trackParams);
  // const solutionParams = {
  //   language: 'eng',
  //   status: fs.readFileSync('solution-status.txt', 'utf-8').trim(),
  //   in_reply_to_id: trackPostResp.data.id,
  //   spoiler_text: `Solution for ${date}`,
  //   sensitive: true,
  //   visibility: 'unlisted',
  // };
}

post().catch((err) => {
  console.error(err);
  process.exit(1);
});
